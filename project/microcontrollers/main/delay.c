/* sensemesh_5.0 - Softwareprojekt Telematik - Prof. Katinka Wolter
 * 
 * Philip Fleischer, Enno Strauß, Dorian Grosch
 * 
 */

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
 
void delay(float seconds) {
	vTaskDelay(seconds * 1000 / portTICK_PERIOD_MS);
}
