package myapps;

import org.apache.kafka.common.serialization.*;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.*;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.Arrays;
import java.util.Base64;

import java.time.Duration;

import java.util.regex.*;

public class Pipe {

    public static void main(String[] args) throws Exception {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-pipe");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

	final StreamsBuilder builder = new StreamsBuilder();

    final KStream<String, String> source = builder
	    .stream("sensemesh", Consumed.with(Serdes.String(), Serdes.String()))
	    .map((key,value) -> KeyValue.pair(key, extract_yaml(value)))
        .selectKey((key,value) -> extract_device_id(value))
        ;

    KTable<String, String> avg_temperature = source
        .map((key,value) -> KeyValue.pair(key, extract_temperature(value)))
        .groupByKey()
        .reduce((value1, value2) -> { 
            return /* "avg temperature: " + */ Float.toString((Float.parseFloat(value1) + Float.parseFloat(value2)) / 2); 
        } )
        ;

    KTable<String, String> avg_humidity = source
        .map((key,value) -> KeyValue.pair(key, extract_humidity(value)))
        .groupByKey()
        .reduce((value1, value2) -> { 
            return /* "avg humidity: " + */ Float.toString((Float.parseFloat(value1) + Float.parseFloat(value2)) / 2); 
        } )
        ;

    KTable<String, String> avg_pressure = source
        .map((key,value) -> KeyValue.pair(key, extract_pressure(value)))
        .groupByKey()
        .reduce((value1, value2) -> { 
            return /* "avg pressure: " + */ Float.toString((Float.parseFloat(value1) + Float.parseFloat(value2)) / 2); 
        } )
        ;
    
    KTable<String, String> joined = avg_temperature.join(avg_humidity,
        (leftValue, rightValue) -> 
            "avg temperature = " + leftValue + " °C | avg humidity = " + rightValue + " %"
    );

    joined = joined.join(avg_pressure,
        (leftValue, rightValue) -> 
            leftValue + " | avg pressure = " + rightValue + " hP"
    );  
    
    joined
        .toStream()
        .to("sensemesh-output", Produced.with(Serdes.String(), Serdes.String()));
    
    final Topology topology = builder.build();

    final KafkaStreams streams = new KafkaStreams(topology, props);
    final CountDownLatch latch = new CountDownLatch(1);

    // attach shutdown handler to catch control-c
    Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
        @Override
        public void run() {
            streams.close();
            latch.countDown();
        }
    });

    try {
        streams.start();
        latch.await();
    } catch (Throwable e) {
        System.exit(1);
    }

    System.exit(0);
}

	static String extract_yaml(String msg) {
		return new String(Base64.getDecoder().decode(msg.substring(55, msg.length()-2)));
	}

	static String extract_device_id(String msg) {
		 Pattern p = Pattern.compile("(?<=device: )[a-zA-Z0-9_]+");
		 Matcher m = p.matcher(msg);
		 if (m.find()) {
			return m.group(0);
		 } else {
			return "no_device";
		 }
	}

    static String extract_temperature(String msg) {
         Pattern p = Pattern.compile("(?<=temperature: )[0-9.]+");
         Matcher m = p.matcher(msg);
         if (m.find()) {
                return m.group(0);
         } else {
                return "no_device";
         }
    }

    static String extract_humidity(String msg) {
         Pattern p = Pattern.compile("(?<=humidity: )[0-9.]+");
         Matcher m = p.matcher(msg);
         if (m.find()) {
                return m.group(0);
         } else {
                return "no_device";
         }
    }

    static String extract_pressure(String msg) {
         Pattern p = Pattern.compile("(?<=pressure: )[0-9.]+");
         Matcher m = p.matcher(msg);
         if (m.find()) {
                return m.group(0);
         } else {
                return "no_device";
         }
    }

}
