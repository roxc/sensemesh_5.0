/* sensemesh_5.0 - Softwareprojekt Telematik - Prof. Katinka Wolter
 * 
 * Philip Fleischer, Enno Strauß, Dorian Grosch
 * 
 */

#include "esp_log.h"
#include "mqtt_client.h"

#include "device_id.h"
#include "mqtt.h"

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
    // esp_mqtt_client_handle_t client = event->client;
    // your_context_t *context = event->context;
    
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(DEVICE_ID, "MQTT_EVENT_CONNECTED");
            break;
            
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(DEVICE_ID, "MQTT_EVENT_DISCONNECTED");
            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(DEVICE_ID, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            break;
            
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(DEVICE_ID, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
            
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(DEVICE_ID, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
            
        case MQTT_EVENT_DATA:
            ESP_LOGI(DEVICE_ID, "MQTT_EVENT_DATA");
            printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
            printf("DATA=%.*s\r\n", event->data_len, event->data);
            break;
            
        case MQTT_EVENT_ERROR:
            ESP_LOGI(DEVICE_ID, "MQTT_EVENT_ERROR");
            break;
            
        default:
            ESP_LOGI(DEVICE_ID, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}

esp_mqtt_client_handle_t mqtt_start_client(void)
{
    esp_mqtt_client_config_t mqtt_cfg = {
        .uri = BROKER,
        .event_handle = mqtt_event_handler,
        // .user_context = (void *)your_context
    };

    esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_start(client);
    
    return client;
}

