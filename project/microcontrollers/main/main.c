/* sensemesh_5.0 - Softwareprojekt Telematik - Prof. Katinka Wolter
 * 
 * Philip Fleischer, Enno Strauß, Dorian Grosch
 * 
 */

#include <stdio.h>

#include "esp_system.h"
#include "esp_mesh.h"
#include "nvs_flash.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "mesh_main.h"
#include "tasks.h"
#include "delay.h"

#define STACK_SIZE 2048

void print_info(void) {
	
	/* Print chip information */
	
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ",
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("silicon revision %d, ", chip_info.revision);

    printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
            (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");
            
}

void app_main(void) {
	
	/* print some system info */
	print_info();
			
	/* initialise tcpip adapter, wifi and mesh */
	mesh_init();
	
	/* wait for mesh to initialize */
	while(!mesh_is_connected()) {
		delay(0.1);
	}
	
	if (esp_mesh_is_root()) {
				
		/* start mqtt client and gathering of messages*/
		xTaskCreate(gather_task, "GATHER_TASK", 5096, NULL, tskIDLE_PRIORITY, NULL);
		
		/* start sensing and sending messages to the root node */
		xTaskCreate(sense_task, "SENSE_TASK", STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL);
		
	} else {
		
		/* start sensing and sending messages to the root node */
		xTaskCreate(sense_task, "SENSE_TASK", STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL);

	}
	
}

