# Softwareprojekt Telematik - "sensemesh_5.0"

**Thema:** Sensordatenerfassung und -analyse durch ein Mikrocontroller-Meshnetz und Apache Kafka als Feldstudie eines "Smart IoT"-Szenarios

## Hardware

* [ESP32-Mikrocontroller](https://www.espressif.com/en/products/hardware/esp32/overview)
* [BME280 Sensor](https://www.olimex.com/Products/Modules/Sensors/MOD-BME280/open-source-hardware)

## Software

* [ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/latest/get-started/) + [ESP-MDF](https://docs.espressif.com/projects/esp-mdf/en/latest/get-started/)
* [graphite](https://github.com/graphite-project)

## Wiring

### I2C-sensors

GND -> GND

VDD -> 3V3

SDA -> 15

SCL -> 2

### MQ-7

GND -> GND

VCC -> 3V3

AO -> 34
## Cloning

clone the repo with the `--recursive` option or run `git submodule update --init` to clone the correct version of the esp-idf repo 
## Building

* [Setup the Toolchain](https://docs.espressif.com/projects/esp-idf/en/latest/get-started/index.html#get-started-setup-toolchain)
    * (You can skip the ESP-IDF part, since it's already in the repo)
    * The project depends on an older ESP-IDF and it is incompatible with newer toolchains, so use the one in /your/path/here/sensemesh_5.0/esp/ instead.
* Set IDF_PATH and MDF_PATH as follows in your ~/.profile
```[bash]
# set IDF_PATH for the ESP32 IDF
export IDF_PATH=/your/path/here/sensemesh_5.0/esp-mdf/esp-idf

# set IDF_PATH for the ESP32 MDF
export MDF_PATH=/your/path/here/sensemesh_5.0/esp-mdf
```
* Execute `make menuconfig` and set approprate "Serial flasher config" settings
* Execute `make -j4 all` in the /sensemesh_5.0/project/microcontrollers/ directory to build the project
* Execute `make flash` in the /sensemesh_5.0/project/microcontrollers/ directory to flash the project

## Testing

1. Change the file [device_id.h](./project/microcontrollers/main/include/device_id.h) to a unique device ID string
2. Flash board
3. Repeat 1 & 2 for every board
4. Connect boards with power sources
5. Observe the topic `/sensemesh` on broker.hivemq.com:1883

## Internal cooperation

[Project Documentation](https://www.overleaf.com/5553336217mvtvtrxrzmtm)
