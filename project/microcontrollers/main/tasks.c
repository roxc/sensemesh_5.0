/* sensemesh_5.0 - Softwareprojekt Telematik - Prof. Katinka Wolter
 * 
 * Philip Fleischer, Enno Strauß, Dorian Grosch
 * 
 */

#include "esp_mesh.h"
#include "esp_mesh_internal.h"

#include "esp_log.h"
#include "mqtt_client.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "mqtt.h"
#include "device_id.h"
#include "delay.h"
#include "sense.h"
#include "mq7.h"

/*******************************************************
 *                Constants
 *******************************************************/
#define RX_SIZE          (1500)
#define TX_SIZE          (1460)

/*******************************************************
 *                Variable Definitions
 *******************************************************/
 
static uint8_t tx_buf[TX_SIZE] = { 0, };
static uint8_t rx_buf[RX_SIZE] = { 0, };

/*******************************************************
 *                Function Definitions
 *******************************************************/
 
void sense_task(void * pvParameters) {
	
	/* declare sense data variables */
		
	float temperature;
	float humidity;
	float pressure;
    int co2;
	
	/* initialize i2c and sensors */
	
	i2c_master_init();

	for( ;; ) {
		
		/* gather sense data from all sensors */
		
		bme280_read_values(&pressure, &temperature, &humidity);
        mq7_get_value(&co2);
		
		/* construct message */
		
		sprintf((char*)tx_buf, 
			"- {device: %s, temperature: %0.1f, humidity: %0.1f, pressure: %0.1f, co2: %d}", 
			DEVICE_ID, 
			temperature, 
			humidity, 
			pressure,
            co2);
		
		/* send message to root */

		esp_err_t err;
		
		mesh_data_t data;
		data.data = tx_buf;
		data.size = sizeof(tx_buf);
		data.proto = MESH_PROTO_BIN;
		
		err = esp_mesh_send(NULL, &data, 0, NULL, 0);
		printf("DATA SENT TO ROOT with err:%d\nPAYLOAD: %s\n", err, tx_buf);
		
		delay(5);
	}
	
	vTaskDelete(NULL);
	
}

void gather_task(void * pvParameters) {
	
	/* start mqtt client */
	
	esp_mqtt_client_handle_t client = mqtt_start_client();
	
    esp_err_t err;
    mesh_addr_t from;
    mesh_data_t data;
    int flag = 0;
    data.data = rx_buf;
    data.size = RX_SIZE;

    for( ;; ) {
		
		/* receive message from mesh */
		
        data.size = RX_SIZE;
        err = esp_mesh_recv(&from, &data, portMAX_DELAY, &flag, NULL, 0);
        
        if (err != ESP_OK || !data.size) {
            ESP_LOGE(DEVICE_ID, "err:0x%x, size:%d", err, data.size);
            continue;
        }
        
        /* send message over mqtt */
        
        int msg_id = esp_mqtt_client_publish(client, TOPIC, (const char*) data.data, 0, 1, 0);
		ESP_LOGI(DEVICE_ID, "sent publish successful, msg_id=%d", msg_id);
        
    }
    
    vTaskDelete(NULL);
    
}
