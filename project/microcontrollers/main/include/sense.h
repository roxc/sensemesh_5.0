#ifndef __SENSE_H__
#define __SENSE_H__

void i2c_master_init();
void bme280_read_values(float *pressure, float *temperature, float *humidity);

#endif /* __SENSE_H__ */

